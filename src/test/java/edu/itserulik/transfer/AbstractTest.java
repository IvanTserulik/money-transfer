package edu.itserulik.transfer;

import com.google.inject.Guice;
import com.google.inject.Key;
import de.flapdoodle.embed.mongo.MongodExecutable;
import edu.itserulik.transfer.dao.GenericDao;
import edu.itserulik.transfer.db.EmbeddedMongoStarter;
import edu.itserulik.transfer.http.NettyServer;
import edu.itserulik.transfer.model.document.Account;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;

class AbstractTest {

    private static NettyServer nettyServer;
    private static MongodExecutable mongodExecutable;
    static GenericDao<Account> dao;

    @BeforeAll
    static void setup() throws IOException {
        var injector = Guice.createInjector(new TestModule());

        var mongoStarter = injector.getInstance(EmbeddedMongoStarter.class);
        mongodExecutable = mongoStarter.embeddedMongoExecutable();
        mongodExecutable.start();

        nettyServer = injector.getInstance(NettyServer.class);
        new Thread(() -> nettyServer.getServer()
                .onDispose()
                .block()).start();

        dao = injector.getInstance(new Key<GenericDao<Account>>() {
        });
    }

    @AfterAll
    static void postTest() {
        if (mongodExecutable != null) {
            mongodExecutable.stop();
        }
    }

}
