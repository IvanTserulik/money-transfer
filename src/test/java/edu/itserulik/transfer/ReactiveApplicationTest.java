package edu.itserulik.transfer;

import edu.itserulik.transfer.model.document.Account;
import edu.itserulik.transfer.model.document.Person;
import edu.itserulik.transfer.model.dto.TransferDto;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.with;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

class ReactiveApplicationTest extends AbstractTest {

    private static final String NAME1 = "Ivan";
    private static final String NAME2 = "Ekaterina";

    @Test
    void getAccount() {
        ObjectId objectId = ObjectId.get();
        dao.save(createAccount(objectId, BigDecimal.TEN, NAME1)).block();

        get("/api/account?id=" + objectId.toHexString()).then().statusCode(200)
                .assertThat()
                .body("party.firstName", equalTo(NAME1));
    }

    @Test
    void transferMoney_success() {
        ObjectId accountId1 = ObjectId.get();
        dao.save(createAccount(accountId1, BigDecimal.ONE, NAME1)).block();

        ObjectId accountId2 = ObjectId.get();
        dao.save(createAccount(accountId2, BigDecimal.TEN, NAME2)).block();


        with().body(
                TransferDto.builder()
                        .accountFromId(accountId1.toHexString())
                        .accountToId(accountId2.toHexString())
                        .sum(BigDecimal.ONE)
                        .build())
                .when()
                .request("POST", "/api/transfer")
                .then()
                .statusCode(200)
                .assertThat()
                .body("balance", hasItems(0, 11));
    }

    @Test
    void transferMoney_failNotEnoughMoney() {
        ObjectId accountId1 = ObjectId.get();
        dao.save(createAccount(accountId1, BigDecimal.ONE, NAME1)).block();

        ObjectId accountId2 = ObjectId.get();
        dao.save(createAccount(accountId2, BigDecimal.ONE, NAME2)).block();

        with().body(
                TransferDto.builder()
                        .accountFromId(accountId1.toHexString())
                        .accountToId(accountId2.toHexString())
                        .sum(BigDecimal.TEN)
                        .build())
                .when()
                .request("POST", "/api/transfer")
                .then()
                .statusCode(400)
                .assertThat()
                .body("message", equalTo("Not enough money to transfer from account"));
    }

    private Account createAccount(ObjectId id, BigDecimal balance, String firstName) {
        return Account.builder()
                .id(id)
                .balance(balance)
                .party(Person.builder()
                        .firstName(firstName)
                        .build())
                .build();
    }

}
