package edu.itserulik.transfer.db.impl;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.mongodb.reactivestreams.client.MongoCollection;
import edu.itserulik.transfer.db.CollectionClient;
import edu.itserulik.transfer.db.MongoProvider;

public class CollectionClientImpl implements CollectionClient {

    private String databaseName;
    private String collectionName;
    private MongoProvider mongoProvider;

    @Inject
    public CollectionClientImpl(MongoProvider mongoProvider,
                                @Named("mongo.databaseName") String databaseName,
                                @Named("mongo.collectionName") String collectionName) {
        this.databaseName = databaseName;
        this.collectionName = collectionName;
        this.mongoProvider = mongoProvider;
    }

    @Override
    public <T> MongoCollection<T> getMongoCollection(Class<T> clazz) {
        return mongoProvider.getMongoClient()
                .getDatabase(databaseName)
                .getCollection(collectionName, clazz);
    }

}
