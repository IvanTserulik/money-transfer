package edu.itserulik.transfer.db;

import com.google.inject.AbstractModule;
import edu.itserulik.transfer.common.PropertiesModule;
import edu.itserulik.transfer.db.impl.CollectionClientImpl;
import edu.itserulik.transfer.db.impl.MongoProviderImpl;

public class DbModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new PropertiesModule());

        bind(MongoProvider.class).to(MongoProviderImpl.class);

        bind(CollectionClient.class).to(CollectionClientImpl.class);
    }
}
