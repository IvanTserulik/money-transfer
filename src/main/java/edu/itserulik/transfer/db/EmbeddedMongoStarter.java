package edu.itserulik.transfer.db;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

import java.io.IOException;

public class EmbeddedMongoStarter {

    private String ip;
    private int port;

    @Inject
    public EmbeddedMongoStarter(@Named("de.flapdoodle.ip") String ip,
                                @Named("de.flapdoodle.port") int port) {
        this.ip = ip;
        this.port = port;
    }

    public MongodExecutable embeddedMongoExecutable() throws IOException {
        MongodStarter starter = MongodStarter.getDefaultInstance();

        IMongodConfig mongoConfig = new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(ip, port, Network.localhostIsIPv6()))
                .build();

        return starter.prepare(mongoConfig);
    }
}
