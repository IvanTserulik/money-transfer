package edu.itserulik.transfer.service.impl;

import com.google.inject.Inject;
import edu.itserulik.transfer.common.exception.NotEnoughMoneyException;
import edu.itserulik.transfer.dao.GenericDao;
import edu.itserulik.transfer.model.document.Account;
import edu.itserulik.transfer.model.dto.TransferDto;
import edu.itserulik.transfer.service.TransferService;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TransferServiceImpl implements TransferService {

    private GenericDao<Account> accountDao;

    @Inject
    public TransferServiceImpl(GenericDao<Account> accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public Mono<List<Account>> transferMoney(TransferDto dto) {
        /*
        I'm glad that you finally arrived this part.
        Unfortunately MongoDB supports sessions only when has a few replicas.
        No one wants replicas.
        Thus, everything goes non-transactionally.
        Locking in the reactive app is very bad, so.. we have what we have.
         */

        var accountFromMono = accountDao.getById(dto.getAccountFromId());
        var accountToMono = accountDao.getById(dto.getAccountToId());

        var updateFromMono = accountFromMono.flatMap(account -> {
            var currentSum = account.getBalance();
            currentSum = currentSum.subtract(dto.getSum());
            if (currentSum.compareTo(BigDecimal.ZERO) < 0) {
                throw new NotEnoughMoneyException();
            }
            account.setBalance(currentSum);
            return accountDao.update(account);
        });

        var updateToMono = accountToMono.flatMap(account -> {
            account.setBalance(account.getBalance().add(dto.getSum()));
            return accountDao.update(account);
        });

        var newAccountFromMono = accountDao.getById(dto.getAccountToId());
        var newAccountToMono = accountDao.getById(dto.getAccountFromId());

        return updateFromMono.then(updateToMono)
                .flatMap(v -> newAccountFromMono)
                .flatMap(accountFrom -> newAccountToMono.map(accountTo -> {
                    var list = new ArrayList<Account>();
                    list.add(accountFrom);
                    list.add(accountTo);
                    return list;
                }));
    }
}
